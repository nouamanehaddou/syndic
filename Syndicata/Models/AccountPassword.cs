﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Syndicata.Models
{
    public class AccountPassword
    {
        [Required]
        [DisplayName("Mot de passe actuel :")]
        public string PasswordActuel { get; set; }
        
        [Required]
        [DisplayName("Nouveau Mot de passe :")]
        public string NouveauPassword { get; set; }

        [Required]
        [Compare("NouveauPassword")]
        [DisplayName("Confirmer le nouveau Mot de passe :")]
        public string ConfirmerNouveauPassword { get; set; }
    }
}