﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Syndicata.Data;
using Syndicata.Models;

namespace Syndicata.Controllers
{
    public class AppartementsController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: Appartements
        public ActionResult Index()
        {
            var appartements = db.Appartements.Include(a => a.Propritaire).Include(a => a.Residence);
            return View(appartements.ToList());
        }

        // GET: Appartements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appartement appartement = db.Appartements.Include(_ => _.Propritaire).Include(_ => _.Residence).SingleOrDefault(_ => _.Id == id);
            if (appartement == null)
            {
                return HttpNotFound();
            }
            return View(appartement);
        }

        // GET: Appartements/Create
        public ActionResult Create()
        {
            ViewBag.PropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName");
            ViewBag.ResidenceId = new SelectList(db.Coproprietes, "Id", "Nom");
            return View();
        }

        // POST: Appartements/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NumeroAppartement,Surface,Etage,ResidenceId,PropritaireId")] Appartement appartement)
        {
            if (ModelState.IsValid)
            {
                db.Appartements.Add(appartement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName");
            ViewBag.ResidenceId = new SelectList(db.Coproprietes, "Id", "Nom", appartement.ResidenceId);
            return View(appartement);
        }

        // GET: Appartements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appartement appartement = db.Appartements.Find(id);
            if (appartement == null)
            {
                return HttpNotFound();
            }
            ViewBag.PropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName");
            ViewBag.ResidenceId = new SelectList(db.Coproprietes, "Id", "Nom", appartement.ResidenceId);
            return View(appartement);
        }

        // POST: Appartements/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NumeroAppartement,Surface,Etage,ResidenceId,PropritaireId")] Appartement appartement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(appartement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "FullName");
            ViewBag.ResidenceId = new SelectList(db.Coproprietes, "Id", "Nom", appartement.ResidenceId);
            return View(appartement);
        }

        // GET: Appartements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appartement appartement = db.Appartements.Find(id);
            if (appartement == null)
            {
                return HttpNotFound();
            }
            return View(appartement);
        }

        // POST: Appartements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Appartement appartement = db.Appartements.Find(id);
            db.Appartements.Remove(appartement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
