﻿using Syndicata.Data;
using Syndicata.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Syndicata.Controllers
{
    public class AccountController : Controller
    {
        private SyndicataContext db = new SyndicataContext();
       
        public ActionResult Login()
        {
            if(Session["userInfo"] != null)
            {
                return RedirectToAction("Index", "Home", null);
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session["userInfo"] = null;
            return RedirectToAction("Index", "Home", null);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var person = db.People.FirstOrDefault(_ => _.Email.Equals(loginViewModel.Email) && _.Password.Equals(loginViewModel.Password));
                    if (person == null)
                    {
                        ViewBag.Error = "donnés invalid, veuillez ressayer !";
                        return View();
                    }
                    ViewBag.Succes = "Utilisateur connecté avec succes";
                    Session["userInfo"] = person;
                    return RedirectToAction("Index", "Home", null);
                }
                return View(loginViewModel);
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Profil()
        {
            Person user = Session["userInfo"] as Person;
            if(user == null)
            {
                return View("Login");
            }
            return View(user);
        }


        public ActionResult EditPassword()
        {
            Person user = Session["userInfo"] as Person;
            if (user == null)
            {
                return View("Login");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPassword(AccountPassword accountPassword)
        {
            if(ModelState.IsValid)
            {
                Person user = Session["userInfo"] as Person;
                if (user == null)
                {
                    return View("Login");
                }
                if (accountPassword.PasswordActuel != user.Password)
                {
                    ViewBag.Error = "le mot de passe actuel entré n'est pas correct";
                    return View(accountPassword);
                }

                user.Password = accountPassword.NouveauPassword;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                Session["userInfo"] = user;
                return RedirectToAction("Profil");
            }
            return View(accountPassword);
        }

    }
}
