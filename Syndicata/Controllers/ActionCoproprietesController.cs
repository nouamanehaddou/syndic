﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Syndicata.Data;
using Syndicata.Models;

namespace Syndicata.Controllers
{
    public class ActionCoproprietesController : Controller
    {
        private SyndicataContext db = new SyndicataContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Person user = Session["userInfo"] as Person;
            if (user == null || !Helper.CheckAutorization(actionName, controllerName, user.RoleId))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NoAccess"}
                    });
                return;
            }
        }

        // GET: ActionCoproprietes
        public ActionResult Index()
        {
            Person user = Session["userInfo"] as Person;
            var actions = db.Actions
                .Include(a => a.Copropritaire)
                .Include(a => a.SyndicReporteur)
                .Where(_ => _.CopropritaireId == user.Id || _.SyndicReporteurId == user.Id);
            return View(actions.ToList());
        }

        // GET: ActionCoproprietes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionCopropriete actionCopropriete = db.Actions
                .Include(_ => _.SyndicReporteur)
                .Include(_ => _.Copropritaire)
                .SingleOrDefault(_ => _.Id == id);
            if (actionCopropriete == null)
            {
                return HttpNotFound();
            }
            return View(actionCopropriete);
        }

        // GET: ActionCoproprietes/Create
        public ActionResult Create()
        {
            ViewBag.CopropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "fullName");
            return View();
        }

        // POST: ActionCoproprietes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Libelle,Motif,ActionType,SyndicReporteurId,CopropritaireId")] ActionCopropriete actionCopropriete)
        {
            if (ModelState.IsValid)
            {
                Person user = Session["userInfo"] as Person;
                actionCopropriete.SyndicReporteurId = user.Id;
                db.Actions.Add(actionCopropriete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CopropritaireId = new SelectList(db.People.Where(_ => _.RoleId == Role.Copropritaire), "Id", "fullName", actionCopropriete.CopropritaireId);
            return View(actionCopropriete);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
