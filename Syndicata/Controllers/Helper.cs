﻿using Syndicata.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Syndicata.Controllers
{
    public static class Helper
    {

        public static bool CheckAutorization(string action, string controller, Role role)
        {
            switch (role)
            {
                case Role.Syndic:
                    return true;
                case Role.Copropritaire:
                    if (controller == "Coproprietes" && action == "Details") return true;
                    if (controller == "Coproprietes" && action == "Index") return true;
                    if (controller == "AssembleGenerals" && action == "Index") return true;
                    if (controller == "AssembleGenerals" && action == "Details") return true;
                    if (controller == "ActionCoproprietes" && action == "Details") return true;
                    if (controller == "ActionCoproprietes" && action == "Index") return true;
                    if (controller == "Paymants" && action == "Index") return true;
                    if (controller == "Paymants" && action == "Details") return true;
                    break;
                default:
                    return false;
            }
            return false;
        }
    }
}